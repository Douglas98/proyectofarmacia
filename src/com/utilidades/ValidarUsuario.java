/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utilidades;

import com.conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author samue
 */
public class ValidarUsuario extends Conexion{
 
    
    public String validarUsuario(String caja, String caja1 )
    { String valor = "";
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "select nombreTipoUsuario from tipoUsuario tu "
                    + "inner join usuario u "
                    + "on u.idTipoUsuario = tu.idTipoUsuario "
                    + "where u.usuario = '"+caja+"'";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
              if(res.getString("nombreTipoUsuario").equals(null))
              {
                  valor = "";
              }else
              {
                  valor = res.getString("nombreTipoUsuario");
              }
            }
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al validar usuario: " +e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return valor;
    }
}
