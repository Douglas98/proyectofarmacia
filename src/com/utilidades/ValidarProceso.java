/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utilidades;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author samue
 */
public class ValidarProceso {
    
    
    public boolean validarOperacion(String operacion)
    {
        int siNo = JOptionPane.showConfirmDialog(null, "Desea "+ operacion+" este elemento?", "CONFIRMACION", JOptionPane.YES_NO_OPTION);
   if(siNo == 0)
   {
       return true;
   }else
   {
       return false;
   }
    }
    
    
    public boolean validarIndice(JComboBox combo)
    {
        if(combo.getSelectedIndex()>0)
        {
         return true;   
        }else
        {
            JOptionPane.showMessageDialog(null, "Por favor seleccione un valor Valido", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
