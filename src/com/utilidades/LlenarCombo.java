/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utilidades;

import com.conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author samue
 */
public class LlenarCombo extends Conexion{
    
    
    public void llenarCombo(String tabla, String campo, JComboBox combo)
    {
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "select "+campo+" from "+tabla;
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            combo.removeAllItems();
            combo.addItem(".:Seleccionar:.");
            while(res.next())
            {
                combo.addItem(res.getString(campo));
            }
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al llenar ComboBox: " + e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    
     public void llenarComboDinamico(String tabla, String campo,String campo2, int valor, JComboBox combo)
    {
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "select "+campo+" from "+tabla+" where "+campo2+" = '"+valor+"'";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            combo.removeAllItems();
            combo.addItem(".:Seleccionar:.");
            while(res.next())
            {
                combo.addItem(res.getString(campo));
            }
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al llenar ComboBox: " + e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    
    
    
       public int valorCombo(String tabla, String campo, String campo2,  String dato)
    {
        int valor = 0;
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "select "+campo+" from "+tabla+" where "+campo2+" = '"+dato+"'";
            PreparedStatement pre = this.getCon().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                valor = res.getInt(campo);
            }
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error al llenar ComboBox: " + e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return valor;
    }
}
