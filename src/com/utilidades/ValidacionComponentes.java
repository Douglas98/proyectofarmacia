/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utilidades;

import java.awt.event.KeyEvent;
import javax.swing.JTextField;

/**
 *
 * @author samue
 */
public class ValidacionComponentes {
    
    
    public void validarDigitos(KeyEvent evt)
    {
        Character s = evt.getKeyChar();
        if(!Character.isDigit(s))
        {
            evt.consume();
        }
    }
    
    
    public void validarTexto(KeyEvent evt)
    {
        Character s = evt.getKeyChar();
        if(!Character.isLetter(s) && s != KeyEvent.VK_SPACE)
        {
            evt.consume();
        }
    }
    
    
    public void validarDecimales(KeyEvent evt, JTextField texto)
    {
        Character s = evt.getKeyChar();
        if(!Character.isDigit(s) && s != '.')
        {
            evt.consume();
        }
        if(s.equals('.') && texto.getText().contains("."))
        {
            evt.consume();
        }
    }
}
