/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author samue
 */
@Entity
@Table(name = "tipoventa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipoventa.findAll", query = "SELECT t FROM Tipoventa t")
    , @NamedQuery(name = "Tipoventa.findByIdTipoVenta", query = "SELECT t FROM Tipoventa t WHERE t.idTipoVenta = :idTipoVenta")
    , @NamedQuery(name = "Tipoventa.findByNombreTipoventa", query = "SELECT t FROM Tipoventa t WHERE t.nombreTipoventa = :nombreTipoventa")})
public class Tipoventa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoVenta")
    private Integer idTipoVenta;
    @Basic(optional = false)
    @Column(name = "nombreTipoventa")
    private String nombreTipoventa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoVenta")
    private List<Venta> ventaList;

    public Tipoventa() {
    }

    public Tipoventa(Integer idTipoVenta) {
        this.idTipoVenta = idTipoVenta;
    }

    public Tipoventa(Integer idTipoVenta, String nombreTipoventa) {
        this.idTipoVenta = idTipoVenta;
        this.nombreTipoventa = nombreTipoventa;
    }

    public Integer getIdTipoVenta() {
        return idTipoVenta;
    }

    public void setIdTipoVenta(Integer idTipoVenta) {
        this.idTipoVenta = idTipoVenta;
    }

    public String getNombreTipoventa() {
        return nombreTipoventa;
    }

    public void setNombreTipoventa(String nombreTipoventa) {
        this.nombreTipoventa = nombreTipoventa;
    }

    @XmlTransient
    public List<Venta> getVentaList() {
        return ventaList;
    }

    public void setVentaList(List<Venta> ventaList) {
        this.ventaList = ventaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoVenta != null ? idTipoVenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipoventa)) {
            return false;
        }
        Tipoventa other = (Tipoventa) object;
        if ((this.idTipoVenta == null && other.idTipoVenta != null) || (this.idTipoVenta != null && !this.idTipoVenta.equals(other.idTipoVenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.Tipoventa[ idTipoVenta=" + idTipoVenta + " ]";
    }
    
}
