/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author samue
 */
@Entity
@Table(name = "detalleventa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalleventa.findAll", query = "SELECT d FROM Detalleventa d")
    , @NamedQuery(name = "Detalleventa.findByIdDetalleventa", query = "SELECT d FROM Detalleventa d WHERE d.idDetalleventa = :idDetalleventa")
    , @NamedQuery(name = "Detalleventa.findByPrecioUnitario", query = "SELECT d FROM Detalleventa d WHERE d.precioUnitario = :precioUnitario")
    , @NamedQuery(name = "Detalleventa.findByCantidad", query = "SELECT d FROM Detalleventa d WHERE d.cantidad = :cantidad")})
public class Detalleventa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDetalleventa")
    private Integer idDetalleventa;
    @Basic(optional = false)
    @Column(name = "precioUnitario")
    private float precioUnitario;
    @Basic(optional = false)
    @Column(name = "cantidad")
    private int cantidad;
    @JoinColumn(name = "idMedicamento", referencedColumnName = "idMedicamento")
    @ManyToOne(optional = false)
    private Medicamento idMedicamento;
    @JoinColumn(name = "idVenta", referencedColumnName = "idVenta")
    @ManyToOne(optional = false)
    private Venta idVenta;

    public Detalleventa() {
    }

    public Detalleventa(Integer idDetalleventa) {
        this.idDetalleventa = idDetalleventa;
    }

    public Detalleventa(Integer idDetalleventa, float precioUnitario, int cantidad) {
        this.idDetalleventa = idDetalleventa;
        this.precioUnitario = precioUnitario;
        this.cantidad = cantidad;
    }

    public Integer getIdDetalleventa() {
        return idDetalleventa;
    }

    public void setIdDetalleventa(Integer idDetalleventa) {
        this.idDetalleventa = idDetalleventa;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Medicamento getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(Medicamento idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public Venta getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Venta idVenta) {
        this.idVenta = idVenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalleventa != null ? idDetalleventa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalleventa)) {
            return false;
        }
        Detalleventa other = (Detalleventa) object;
        if ((this.idDetalleventa == null && other.idDetalleventa != null) || (this.idDetalleventa != null && !this.idDetalleventa.equals(other.idDetalleventa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.Detalleventa[ idDetalleventa=" + idDetalleventa + " ]";
    }
    
}
