/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author samue
 */
@Entity
@Table(name = "tipotarjeta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipotarjeta.findAll", query = "SELECT t FROM Tipotarjeta t")
    , @NamedQuery(name = "Tipotarjeta.findByIdTipoTarjeta", query = "SELECT t FROM Tipotarjeta t WHERE t.idTipoTarjeta = :idTipoTarjeta")
    , @NamedQuery(name = "Tipotarjeta.findByNombreTipoTarjeta", query = "SELECT t FROM Tipotarjeta t WHERE t.nombreTipoTarjeta = :nombreTipoTarjeta")})
public class Tipotarjeta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoTarjeta")
    private Integer idTipoTarjeta;
    @Basic(optional = false)
    @Column(name = "nombreTipoTarjeta")
    private String nombreTipoTarjeta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipotarjeta")
    private List<Tarjeta> tarjetaList;

    public Tipotarjeta() {
    }

    public Tipotarjeta(Integer idTipoTarjeta) {
        this.idTipoTarjeta = idTipoTarjeta;
    }

    public Tipotarjeta(Integer idTipoTarjeta, String nombreTipoTarjeta) {
        this.idTipoTarjeta = idTipoTarjeta;
        this.nombreTipoTarjeta = nombreTipoTarjeta;
    }

    public Integer getIdTipoTarjeta() {
        return idTipoTarjeta;
    }

    public void setIdTipoTarjeta(Integer idTipoTarjeta) {
        this.idTipoTarjeta = idTipoTarjeta;
    }

    public String getNombreTipoTarjeta() {
        return nombreTipoTarjeta;
    }

    public void setNombreTipoTarjeta(String nombreTipoTarjeta) {
        this.nombreTipoTarjeta = nombreTipoTarjeta;
    }

    @XmlTransient
    public List<Tarjeta> getTarjetaList() {
        return tarjetaList;
    }

    public void setTarjetaList(List<Tarjeta> tarjetaList) {
        this.tarjetaList = tarjetaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoTarjeta != null ? idTipoTarjeta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipotarjeta)) {
            return false;
        }
        Tipotarjeta other = (Tipotarjeta) object;
        if ((this.idTipoTarjeta == null && other.idTipoTarjeta != null) || (this.idTipoTarjeta != null && !this.idTipoTarjeta.equals(other.idTipoTarjeta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.Tipotarjeta[ idTipoTarjeta=" + idTipoTarjeta + " ]";
    }
    
}
