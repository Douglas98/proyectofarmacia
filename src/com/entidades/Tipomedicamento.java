/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author samue
 */
@Entity
@Table(name = "tipomedicamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipomedicamento.findAll", query = "SELECT t FROM Tipomedicamento t")
    , @NamedQuery(name = "Tipomedicamento.findByIdTipoMedicamento", query = "SELECT t FROM Tipomedicamento t WHERE t.idTipoMedicamento = :idTipoMedicamento")
    , @NamedQuery(name = "Tipomedicamento.findByNombreTipoMedicamento", query = "SELECT t FROM Tipomedicamento t WHERE t.nombreTipoMedicamento = :nombreTipoMedicamento")})
public class Tipomedicamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoMedicamento")
    private Integer idTipoMedicamento;
    @Basic(optional = false)
    @Column(name = "nombreTipoMedicamento")
    private String nombreTipoMedicamento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoMedicamento")
    private List<Medicamento> medicamentoList;

    public Tipomedicamento() {
    }

    public Tipomedicamento(Integer idTipoMedicamento) {
        this.idTipoMedicamento = idTipoMedicamento;
    }

    public Tipomedicamento(Integer idTipoMedicamento, String nombreTipoMedicamento) {
        this.idTipoMedicamento = idTipoMedicamento;
        this.nombreTipoMedicamento = nombreTipoMedicamento;
    }

    public Integer getIdTipoMedicamento() {
        return idTipoMedicamento;
    }

    public void setIdTipoMedicamento(Integer idTipoMedicamento) {
        this.idTipoMedicamento = idTipoMedicamento;
    }

    public String getNombreTipoMedicamento() {
        return nombreTipoMedicamento;
    }

    public void setNombreTipoMedicamento(String nombreTipoMedicamento) {
        this.nombreTipoMedicamento = nombreTipoMedicamento;
    }

    @XmlTransient
    public List<Medicamento> getMedicamentoList() {
        return medicamentoList;
    }

    public void setMedicamentoList(List<Medicamento> medicamentoList) {
        this.medicamentoList = medicamentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoMedicamento != null ? idTipoMedicamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipomedicamento)) {
            return false;
        }
        Tipomedicamento other = (Tipomedicamento) object;
        if ((this.idTipoMedicamento == null && other.idTipoMedicamento != null) || (this.idTipoMedicamento != null && !this.idTipoMedicamento.equals(other.idTipoMedicamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.Tipomedicamento[ idTipoMedicamento=" + idTipoMedicamento + " ]";
    }
    
}
