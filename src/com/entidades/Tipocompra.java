/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author samue
 */
@Entity
@Table(name = "tipocompra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipocompra.findAll", query = "SELECT t FROM Tipocompra t")
    , @NamedQuery(name = "Tipocompra.findByIdTipoCompra", query = "SELECT t FROM Tipocompra t WHERE t.idTipoCompra = :idTipoCompra")
    , @NamedQuery(name = "Tipocompra.findByNombreTipoCompra", query = "SELECT t FROM Tipocompra t WHERE t.nombreTipoCompra = :nombreTipoCompra")})
public class Tipocompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoCompra")
    private Integer idTipoCompra;
    @Basic(optional = false)
    @Column(name = "nombreTipoCompra")
    private String nombreTipoCompra;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoCompra")
    private List<Compra> compraList;

    public Tipocompra() {
    }

    public Tipocompra(Integer idTipoCompra) {
        this.idTipoCompra = idTipoCompra;
    }

    public Tipocompra(Integer idTipoCompra, String nombreTipoCompra) {
        this.idTipoCompra = idTipoCompra;
        this.nombreTipoCompra = nombreTipoCompra;
    }

    public Integer getIdTipoCompra() {
        return idTipoCompra;
    }

    public void setIdTipoCompra(Integer idTipoCompra) {
        this.idTipoCompra = idTipoCompra;
    }

    public String getNombreTipoCompra() {
        return nombreTipoCompra;
    }

    public void setNombreTipoCompra(String nombreTipoCompra) {
        this.nombreTipoCompra = nombreTipoCompra;
    }

    @XmlTransient
    public List<Compra> getCompraList() {
        return compraList;
    }

    public void setCompraList(List<Compra> compraList) {
        this.compraList = compraList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoCompra != null ? idTipoCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipocompra)) {
            return false;
        }
        Tipocompra other = (Tipocompra) object;
        if ((this.idTipoCompra == null && other.idTipoCompra != null) || (this.idTipoCompra != null && !this.idTipoCompra.equals(other.idTipoCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.Tipocompra[ idTipoCompra=" + idTipoCompra + " ]";
    }
    
}
