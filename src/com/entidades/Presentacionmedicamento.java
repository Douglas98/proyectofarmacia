/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author samue
 */
@Entity
@Table(name = "presentacionmedicamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Presentacionmedicamento.findAll", query = "SELECT p FROM Presentacionmedicamento p")
    , @NamedQuery(name = "Presentacionmedicamento.findByIdPresentacionMedicamento", query = "SELECT p FROM Presentacionmedicamento p WHERE p.idPresentacionMedicamento = :idPresentacionMedicamento")
    , @NamedQuery(name = "Presentacionmedicamento.findByNombrePresentacionMedicamento", query = "SELECT p FROM Presentacionmedicamento p WHERE p.nombrePresentacionMedicamento = :nombrePresentacionMedicamento")})
public class Presentacionmedicamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPresentacionMedicamento")
    private Integer idPresentacionMedicamento;
    @Basic(optional = false)
    @Column(name = "nombrePresentacionMedicamento")
    private String nombrePresentacionMedicamento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPresentacionMedicamento")
    private List<Medicamento> medicamentoList;

    public Presentacionmedicamento() {
    }

    public Presentacionmedicamento(Integer idPresentacionMedicamento) {
        this.idPresentacionMedicamento = idPresentacionMedicamento;
    }

    public Presentacionmedicamento(Integer idPresentacionMedicamento, String nombrePresentacionMedicamento) {
        this.idPresentacionMedicamento = idPresentacionMedicamento;
        this.nombrePresentacionMedicamento = nombrePresentacionMedicamento;
    }

    public Integer getIdPresentacionMedicamento() {
        return idPresentacionMedicamento;
    }

    public void setIdPresentacionMedicamento(Integer idPresentacionMedicamento) {
        this.idPresentacionMedicamento = idPresentacionMedicamento;
    }

    public String getNombrePresentacionMedicamento() {
        return nombrePresentacionMedicamento;
    }

    public void setNombrePresentacionMedicamento(String nombrePresentacionMedicamento) {
        this.nombrePresentacionMedicamento = nombrePresentacionMedicamento;
    }

    @XmlTransient
    public List<Medicamento> getMedicamentoList() {
        return medicamentoList;
    }

    public void setMedicamentoList(List<Medicamento> medicamentoList) {
        this.medicamentoList = medicamentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPresentacionMedicamento != null ? idPresentacionMedicamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Presentacionmedicamento)) {
            return false;
        }
        Presentacionmedicamento other = (Presentacionmedicamento) object;
        if ((this.idPresentacionMedicamento == null && other.idPresentacionMedicamento != null) || (this.idPresentacionMedicamento != null && !this.idPresentacionMedicamento.equals(other.idPresentacionMedicamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entidades.Presentacionmedicamento[ idPresentacionMedicamento=" + idPresentacionMedicamento + " ]";
    }
    
}
