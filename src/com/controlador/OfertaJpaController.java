/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Medicamento;
import com.entidades.Oferta;
import com.entidades.Sucursal;
import com.entidades.Tipocliente;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class OfertaJpaController implements Serializable {

    public OfertaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public OfertaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Oferta oferta) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Medicamento idMedicamento = oferta.getIdMedicamento();
            if (idMedicamento != null) {
                idMedicamento = em.getReference(idMedicamento.getClass(), idMedicamento.getIdMedicamento());
                oferta.setIdMedicamento(idMedicamento);
            }
            Sucursal idSucursal = oferta.getIdSucursal();
            if (idSucursal != null) {
                idSucursal = em.getReference(idSucursal.getClass(), idSucursal.getIdSucursal());
                oferta.setIdSucursal(idSucursal);
            }
            Tipocliente idTipoCliente = oferta.getIdTipoCliente();
            if (idTipoCliente != null) {
                idTipoCliente = em.getReference(idTipoCliente.getClass(), idTipoCliente.getIdTipoCliente());
                oferta.setIdTipoCliente(idTipoCliente);
            }
            em.persist(oferta);
            if (idMedicamento != null) {
                idMedicamento.getOfertaList().add(oferta);
                idMedicamento = em.merge(idMedicamento);
            }
            if (idSucursal != null) {
                idSucursal.getOfertaList().add(oferta);
                idSucursal = em.merge(idSucursal);
            }
            if (idTipoCliente != null) {
                idTipoCliente.getOfertaList().add(oferta);
                idTipoCliente = em.merge(idTipoCliente);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOferta(oferta.getIdOferta()) != null) {
                throw new PreexistingEntityException("Oferta " + oferta + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Oferta oferta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Oferta persistentOferta = em.find(Oferta.class, oferta.getIdOferta());
            Medicamento idMedicamentoOld = persistentOferta.getIdMedicamento();
            Medicamento idMedicamentoNew = oferta.getIdMedicamento();
            Sucursal idSucursalOld = persistentOferta.getIdSucursal();
            Sucursal idSucursalNew = oferta.getIdSucursal();
            Tipocliente idTipoClienteOld = persistentOferta.getIdTipoCliente();
            Tipocliente idTipoClienteNew = oferta.getIdTipoCliente();
            if (idMedicamentoNew != null) {
                idMedicamentoNew = em.getReference(idMedicamentoNew.getClass(), idMedicamentoNew.getIdMedicamento());
                oferta.setIdMedicamento(idMedicamentoNew);
            }
            if (idSucursalNew != null) {
                idSucursalNew = em.getReference(idSucursalNew.getClass(), idSucursalNew.getIdSucursal());
                oferta.setIdSucursal(idSucursalNew);
            }
            if (idTipoClienteNew != null) {
                idTipoClienteNew = em.getReference(idTipoClienteNew.getClass(), idTipoClienteNew.getIdTipoCliente());
                oferta.setIdTipoCliente(idTipoClienteNew);
            }
            oferta = em.merge(oferta);
            if (idMedicamentoOld != null && !idMedicamentoOld.equals(idMedicamentoNew)) {
                idMedicamentoOld.getOfertaList().remove(oferta);
                idMedicamentoOld = em.merge(idMedicamentoOld);
            }
            if (idMedicamentoNew != null && !idMedicamentoNew.equals(idMedicamentoOld)) {
                idMedicamentoNew.getOfertaList().add(oferta);
                idMedicamentoNew = em.merge(idMedicamentoNew);
            }
            if (idSucursalOld != null && !idSucursalOld.equals(idSucursalNew)) {
                idSucursalOld.getOfertaList().remove(oferta);
                idSucursalOld = em.merge(idSucursalOld);
            }
            if (idSucursalNew != null && !idSucursalNew.equals(idSucursalOld)) {
                idSucursalNew.getOfertaList().add(oferta);
                idSucursalNew = em.merge(idSucursalNew);
            }
            if (idTipoClienteOld != null && !idTipoClienteOld.equals(idTipoClienteNew)) {
                idTipoClienteOld.getOfertaList().remove(oferta);
                idTipoClienteOld = em.merge(idTipoClienteOld);
            }
            if (idTipoClienteNew != null && !idTipoClienteNew.equals(idTipoClienteOld)) {
                idTipoClienteNew.getOfertaList().add(oferta);
                idTipoClienteNew = em.merge(idTipoClienteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = oferta.getIdOferta();
                if (findOferta(id) == null) {
                    throw new NonexistentEntityException("The oferta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Oferta oferta;
            try {
                oferta = em.getReference(Oferta.class, id);
                oferta.getIdOferta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The oferta with id " + id + " no longer exists.", enfe);
            }
            Medicamento idMedicamento = oferta.getIdMedicamento();
            if (idMedicamento != null) {
                idMedicamento.getOfertaList().remove(oferta);
                idMedicamento = em.merge(idMedicamento);
            }
            Sucursal idSucursal = oferta.getIdSucursal();
            if (idSucursal != null) {
                idSucursal.getOfertaList().remove(oferta);
                idSucursal = em.merge(idSucursal);
            }
            Tipocliente idTipoCliente = oferta.getIdTipoCliente();
            if (idTipoCliente != null) {
                idTipoCliente.getOfertaList().remove(oferta);
                idTipoCliente = em.merge(idTipoCliente);
            }
            em.remove(oferta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Oferta> findOfertaEntities() {
        return findOfertaEntities(true, -1, -1);
    }

    public List<Oferta> findOfertaEntities(int maxResults, int firstResult) {
        return findOfertaEntities(false, maxResults, firstResult);
    }

    private List<Oferta> findOfertaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Oferta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Oferta findOferta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Oferta.class, id);
        } finally {
            em.close();
        }
    }

    public int getOfertaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Oferta> rt = cq.from(Oferta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
