/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import com.entidades.Departamento;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Municipios;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class DepartamentoJpaController implements Serializable {

    public DepartamentoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public DepartamentoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Departamento departamento) throws PreexistingEntityException, Exception {
        if (departamento.getMunicipiosList() == null) {
            departamento.setMunicipiosList(new ArrayList<Municipios>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Municipios> attachedMunicipiosList = new ArrayList<Municipios>();
            for (Municipios municipiosListMunicipiosToAttach : departamento.getMunicipiosList()) {
                municipiosListMunicipiosToAttach = em.getReference(municipiosListMunicipiosToAttach.getClass(), municipiosListMunicipiosToAttach.getIdMunicipio());
                attachedMunicipiosList.add(municipiosListMunicipiosToAttach);
            }
            departamento.setMunicipiosList(attachedMunicipiosList);
            em.persist(departamento);
            for (Municipios municipiosListMunicipios : departamento.getMunicipiosList()) {
                Departamento oldIdDepartamentoOfMunicipiosListMunicipios = municipiosListMunicipios.getIdDepartamento();
                municipiosListMunicipios.setIdDepartamento(departamento);
                municipiosListMunicipios = em.merge(municipiosListMunicipios);
                if (oldIdDepartamentoOfMunicipiosListMunicipios != null) {
                    oldIdDepartamentoOfMunicipiosListMunicipios.getMunicipiosList().remove(municipiosListMunicipios);
                    oldIdDepartamentoOfMunicipiosListMunicipios = em.merge(oldIdDepartamentoOfMunicipiosListMunicipios);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDepartamento(departamento.getIdDepartamento()) != null) {
                throw new PreexistingEntityException("Departamento " + departamento + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Departamento departamento) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamento persistentDepartamento = em.find(Departamento.class, departamento.getIdDepartamento());
            List<Municipios> municipiosListOld = persistentDepartamento.getMunicipiosList();
            List<Municipios> municipiosListNew = departamento.getMunicipiosList();
            List<String> illegalOrphanMessages = null;
            for (Municipios municipiosListOldMunicipios : municipiosListOld) {
                /*if (!municipiosListNew.contains(municipiosListOldMunicipios)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Municipios " + municipiosListOldMunicipios + " since its idDepartamento field is not nullable.");
                }*/
            }
           /* if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }*/
            List<Municipios> attachedMunicipiosListNew = new ArrayList<Municipios>();
            for (Municipios municipiosListNewMunicipiosToAttach : municipiosListNew) {
                municipiosListNewMunicipiosToAttach = em.getReference(municipiosListNewMunicipiosToAttach.getClass(), municipiosListNewMunicipiosToAttach.getIdMunicipio());
                attachedMunicipiosListNew.add(municipiosListNewMunicipiosToAttach);
            }
            municipiosListNew = attachedMunicipiosListNew;
            departamento.setMunicipiosList(municipiosListNew);
            departamento = em.merge(departamento);
            for (Municipios municipiosListNewMunicipios : municipiosListNew) {
                if (!municipiosListOld.contains(municipiosListNewMunicipios)) {
                    Departamento oldIdDepartamentoOfMunicipiosListNewMunicipios = municipiosListNewMunicipios.getIdDepartamento();
                    municipiosListNewMunicipios.setIdDepartamento(departamento);
                    municipiosListNewMunicipios = em.merge(municipiosListNewMunicipios);
                    if (oldIdDepartamentoOfMunicipiosListNewMunicipios != null && !oldIdDepartamentoOfMunicipiosListNewMunicipios.equals(departamento)) {
                        oldIdDepartamentoOfMunicipiosListNewMunicipios.getMunicipiosList().remove(municipiosListNewMunicipios);
                        oldIdDepartamentoOfMunicipiosListNewMunicipios = em.merge(oldIdDepartamentoOfMunicipiosListNewMunicipios);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = departamento.getIdDepartamento();
                if (findDepartamento(id) == null) {
                    throw new NonexistentEntityException("The departamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamento departamento;
            try {
                departamento = em.getReference(Departamento.class, id);
                departamento.getIdDepartamento();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The departamento with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Municipios> municipiosListOrphanCheck = departamento.getMunicipiosList();
            for (Municipios municipiosListOrphanCheckMunicipios : municipiosListOrphanCheck) {
                //if (illegalOrphanMessages == null) {
                    //illegalOrphanMessages = new ArrayList<String>();
               // }
                //illegalOrphanMessages.add("This Departamento (" + departamento + ") cannot be destroyed since the Municipios " + municipiosListOrphanCheckMunicipios + " in its municipiosList field has a non-nullable idDepartamento field.");
           }
           // if (illegalOrphanMessages != null) {
               // throw new IllegalOrphanException(illegalOrphanMessages);
           // }
            em.remove(departamento);
            em.getTransaction().commit();
            } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Departamento> findDepartamentoEntities() {
        return findDepartamentoEntities(true, -1, -1);
    }

    public List<Departamento> findDepartamentoEntities(int maxResults, int firstResult) {
        return findDepartamentoEntities(false, maxResults, firstResult);
    }

    private List<Departamento> findDepartamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Departamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Departamento findDepartamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Departamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getDepartamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Departamento> rt = cq.from(Departamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
