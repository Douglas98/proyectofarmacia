/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Presentacionmedicamento;
import com.entidades.Proveedor;
import com.entidades.Tipomedicamento;
import com.entidades.Oferta;
import java.util.ArrayList;
import java.util.List;
import com.entidades.Detalleventa;
import com.entidades.Detallecompra;
import com.entidades.Medicamento;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class MedicamentoJpaController implements Serializable {

    public MedicamentoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public MedicamentoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Medicamento medicamento) throws PreexistingEntityException, Exception {
        if (medicamento.getOfertaList() == null) {
            medicamento.setOfertaList(new ArrayList<Oferta>());
        }
        if (medicamento.getDetalleventaList() == null) {
            medicamento.setDetalleventaList(new ArrayList<Detalleventa>());
        }
        if (medicamento.getDetallecompraList() == null) {
            medicamento.setDetallecompraList(new ArrayList<Detallecompra>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Presentacionmedicamento idPresentacionMedicamento = medicamento.getIdPresentacionMedicamento();
            if (idPresentacionMedicamento != null) {
                idPresentacionMedicamento = em.getReference(idPresentacionMedicamento.getClass(), idPresentacionMedicamento.getIdPresentacionMedicamento());
                medicamento.setIdPresentacionMedicamento(idPresentacionMedicamento);
            }
            Proveedor idProveedor = medicamento.getIdProveedor();
            if (idProveedor != null) {
                idProveedor = em.getReference(idProveedor.getClass(), idProveedor.getIdProveedor());
                medicamento.setIdProveedor(idProveedor);
            }
            Tipomedicamento idTipoMedicamento = medicamento.getIdTipoMedicamento();
            if (idTipoMedicamento != null) {
                idTipoMedicamento = em.getReference(idTipoMedicamento.getClass(), idTipoMedicamento.getIdTipoMedicamento());
                medicamento.setIdTipoMedicamento(idTipoMedicamento);
            }
            List<Oferta> attachedOfertaList = new ArrayList<Oferta>();
            for (Oferta ofertaListOfertaToAttach : medicamento.getOfertaList()) {
                ofertaListOfertaToAttach = em.getReference(ofertaListOfertaToAttach.getClass(), ofertaListOfertaToAttach.getIdOferta());
                attachedOfertaList.add(ofertaListOfertaToAttach);
            }
            medicamento.setOfertaList(attachedOfertaList);
            List<Detalleventa> attachedDetalleventaList = new ArrayList<Detalleventa>();
            for (Detalleventa detalleventaListDetalleventaToAttach : medicamento.getDetalleventaList()) {
                detalleventaListDetalleventaToAttach = em.getReference(detalleventaListDetalleventaToAttach.getClass(), detalleventaListDetalleventaToAttach.getIdDetalleventa());
                attachedDetalleventaList.add(detalleventaListDetalleventaToAttach);
            }
            medicamento.setDetalleventaList(attachedDetalleventaList);
            List<Detallecompra> attachedDetallecompraList = new ArrayList<Detallecompra>();
            for (Detallecompra detallecompraListDetallecompraToAttach : medicamento.getDetallecompraList()) {
                detallecompraListDetallecompraToAttach = em.getReference(detallecompraListDetallecompraToAttach.getClass(), detallecompraListDetallecompraToAttach.getIdDetalleCompra());
                attachedDetallecompraList.add(detallecompraListDetallecompraToAttach);
            }
            medicamento.setDetallecompraList(attachedDetallecompraList);
            em.persist(medicamento);
            if (idPresentacionMedicamento != null) {
                idPresentacionMedicamento.getMedicamentoList().add(medicamento);
                idPresentacionMedicamento = em.merge(idPresentacionMedicamento);
            }
            if (idProveedor != null) {
                idProveedor.getMedicamentoList().add(medicamento);
                idProveedor = em.merge(idProveedor);
            }
            if (idTipoMedicamento != null) {
                idTipoMedicamento.getMedicamentoList().add(medicamento);
                idTipoMedicamento = em.merge(idTipoMedicamento);
            }
            for (Oferta ofertaListOferta : medicamento.getOfertaList()) {
                Medicamento oldIdMedicamentoOfOfertaListOferta = ofertaListOferta.getIdMedicamento();
                ofertaListOferta.setIdMedicamento(medicamento);
                ofertaListOferta = em.merge(ofertaListOferta);
                if (oldIdMedicamentoOfOfertaListOferta != null) {
                    oldIdMedicamentoOfOfertaListOferta.getOfertaList().remove(ofertaListOferta);
                    oldIdMedicamentoOfOfertaListOferta = em.merge(oldIdMedicamentoOfOfertaListOferta);
                }
            }
            for (Detalleventa detalleventaListDetalleventa : medicamento.getDetalleventaList()) {
                Medicamento oldIdMedicamentoOfDetalleventaListDetalleventa = detalleventaListDetalleventa.getIdMedicamento();
                detalleventaListDetalleventa.setIdMedicamento(medicamento);
                detalleventaListDetalleventa = em.merge(detalleventaListDetalleventa);
                if (oldIdMedicamentoOfDetalleventaListDetalleventa != null) {
                    oldIdMedicamentoOfDetalleventaListDetalleventa.getDetalleventaList().remove(detalleventaListDetalleventa);
                    oldIdMedicamentoOfDetalleventaListDetalleventa = em.merge(oldIdMedicamentoOfDetalleventaListDetalleventa);
                }
            }
            for (Detallecompra detallecompraListDetallecompra : medicamento.getDetallecompraList()) {
                Medicamento oldIdMedicamentoOfDetallecompraListDetallecompra = detallecompraListDetallecompra.getIdMedicamento();
                detallecompraListDetallecompra.setIdMedicamento(medicamento);
                detallecompraListDetallecompra = em.merge(detallecompraListDetallecompra);
                if (oldIdMedicamentoOfDetallecompraListDetallecompra != null) {
                    oldIdMedicamentoOfDetallecompraListDetallecompra.getDetallecompraList().remove(detallecompraListDetallecompra);
                    oldIdMedicamentoOfDetallecompraListDetallecompra = em.merge(oldIdMedicamentoOfDetallecompraListDetallecompra);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMedicamento(medicamento.getIdMedicamento()) != null) {
                throw new PreexistingEntityException("Medicamento " + medicamento + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Medicamento medicamento) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Medicamento persistentMedicamento = em.find(Medicamento.class, medicamento.getIdMedicamento());
            Presentacionmedicamento idPresentacionMedicamentoOld = persistentMedicamento.getIdPresentacionMedicamento();
            Presentacionmedicamento idPresentacionMedicamentoNew = medicamento.getIdPresentacionMedicamento();
            Proveedor idProveedorOld = persistentMedicamento.getIdProveedor();
            Proveedor idProveedorNew = medicamento.getIdProveedor();
            Tipomedicamento idTipoMedicamentoOld = persistentMedicamento.getIdTipoMedicamento();
            Tipomedicamento idTipoMedicamentoNew = medicamento.getIdTipoMedicamento();
            List<Oferta> ofertaListOld = persistentMedicamento.getOfertaList();
            List<Oferta> ofertaListNew = medicamento.getOfertaList();
            List<Detalleventa> detalleventaListOld = persistentMedicamento.getDetalleventaList();
            List<Detalleventa> detalleventaListNew = medicamento.getDetalleventaList();
            List<Detallecompra> detallecompraListOld = persistentMedicamento.getDetallecompraList();
            List<Detallecompra> detallecompraListNew = medicamento.getDetallecompraList();
            List<String> illegalOrphanMessages = null;
            for (Oferta ofertaListOldOferta : ofertaListOld) {
                if (!ofertaListNew.contains(ofertaListOldOferta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Oferta " + ofertaListOldOferta + " since its idMedicamento field is not nullable.");
                }
            }
            for (Detalleventa detalleventaListOldDetalleventa : detalleventaListOld) {
                if (!detalleventaListNew.contains(detalleventaListOldDetalleventa)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Detalleventa " + detalleventaListOldDetalleventa + " since its idMedicamento field is not nullable.");
                }
            }
            for (Detallecompra detallecompraListOldDetallecompra : detallecompraListOld) {
                if (!detallecompraListNew.contains(detallecompraListOldDetallecompra)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Detallecompra " + detallecompraListOldDetallecompra + " since its idMedicamento field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idPresentacionMedicamentoNew != null) {
                idPresentacionMedicamentoNew = em.getReference(idPresentacionMedicamentoNew.getClass(), idPresentacionMedicamentoNew.getIdPresentacionMedicamento());
                medicamento.setIdPresentacionMedicamento(idPresentacionMedicamentoNew);
            }
            if (idProveedorNew != null) {
                idProveedorNew = em.getReference(idProveedorNew.getClass(), idProveedorNew.getIdProveedor());
                medicamento.setIdProveedor(idProveedorNew);
            }
            if (idTipoMedicamentoNew != null) {
                idTipoMedicamentoNew = em.getReference(idTipoMedicamentoNew.getClass(), idTipoMedicamentoNew.getIdTipoMedicamento());
                medicamento.setIdTipoMedicamento(idTipoMedicamentoNew);
            }
            List<Oferta> attachedOfertaListNew = new ArrayList<Oferta>();
            for (Oferta ofertaListNewOfertaToAttach : ofertaListNew) {
                ofertaListNewOfertaToAttach = em.getReference(ofertaListNewOfertaToAttach.getClass(), ofertaListNewOfertaToAttach.getIdOferta());
                attachedOfertaListNew.add(ofertaListNewOfertaToAttach);
            }
            ofertaListNew = attachedOfertaListNew;
            medicamento.setOfertaList(ofertaListNew);
            List<Detalleventa> attachedDetalleventaListNew = new ArrayList<Detalleventa>();
            for (Detalleventa detalleventaListNewDetalleventaToAttach : detalleventaListNew) {
                detalleventaListNewDetalleventaToAttach = em.getReference(detalleventaListNewDetalleventaToAttach.getClass(), detalleventaListNewDetalleventaToAttach.getIdDetalleventa());
                attachedDetalleventaListNew.add(detalleventaListNewDetalleventaToAttach);
            }
            detalleventaListNew = attachedDetalleventaListNew;
            medicamento.setDetalleventaList(detalleventaListNew);
            List<Detallecompra> attachedDetallecompraListNew = new ArrayList<Detallecompra>();
            for (Detallecompra detallecompraListNewDetallecompraToAttach : detallecompraListNew) {
                detallecompraListNewDetallecompraToAttach = em.getReference(detallecompraListNewDetallecompraToAttach.getClass(), detallecompraListNewDetallecompraToAttach.getIdDetalleCompra());
                attachedDetallecompraListNew.add(detallecompraListNewDetallecompraToAttach);
            }
            detallecompraListNew = attachedDetallecompraListNew;
            medicamento.setDetallecompraList(detallecompraListNew);
            medicamento = em.merge(medicamento);
            if (idPresentacionMedicamentoOld != null && !idPresentacionMedicamentoOld.equals(idPresentacionMedicamentoNew)) {
                idPresentacionMedicamentoOld.getMedicamentoList().remove(medicamento);
                idPresentacionMedicamentoOld = em.merge(idPresentacionMedicamentoOld);
            }
            if (idPresentacionMedicamentoNew != null && !idPresentacionMedicamentoNew.equals(idPresentacionMedicamentoOld)) {
                idPresentacionMedicamentoNew.getMedicamentoList().add(medicamento);
                idPresentacionMedicamentoNew = em.merge(idPresentacionMedicamentoNew);
            }
            if (idProveedorOld != null && !idProveedorOld.equals(idProveedorNew)) {
                idProveedorOld.getMedicamentoList().remove(medicamento);
                idProveedorOld = em.merge(idProveedorOld);
            }
            if (idProveedorNew != null && !idProveedorNew.equals(idProveedorOld)) {
                idProveedorNew.getMedicamentoList().add(medicamento);
                idProveedorNew = em.merge(idProveedorNew);
            }
            if (idTipoMedicamentoOld != null && !idTipoMedicamentoOld.equals(idTipoMedicamentoNew)) {
                idTipoMedicamentoOld.getMedicamentoList().remove(medicamento);
                idTipoMedicamentoOld = em.merge(idTipoMedicamentoOld);
            }
            if (idTipoMedicamentoNew != null && !idTipoMedicamentoNew.equals(idTipoMedicamentoOld)) {
                idTipoMedicamentoNew.getMedicamentoList().add(medicamento);
                idTipoMedicamentoNew = em.merge(idTipoMedicamentoNew);
            }
            for (Oferta ofertaListNewOferta : ofertaListNew) {
                if (!ofertaListOld.contains(ofertaListNewOferta)) {
                    Medicamento oldIdMedicamentoOfOfertaListNewOferta = ofertaListNewOferta.getIdMedicamento();
                    ofertaListNewOferta.setIdMedicamento(medicamento);
                    ofertaListNewOferta = em.merge(ofertaListNewOferta);
                    if (oldIdMedicamentoOfOfertaListNewOferta != null && !oldIdMedicamentoOfOfertaListNewOferta.equals(medicamento)) {
                        oldIdMedicamentoOfOfertaListNewOferta.getOfertaList().remove(ofertaListNewOferta);
                        oldIdMedicamentoOfOfertaListNewOferta = em.merge(oldIdMedicamentoOfOfertaListNewOferta);
                    }
                }
            }
            for (Detalleventa detalleventaListNewDetalleventa : detalleventaListNew) {
                if (!detalleventaListOld.contains(detalleventaListNewDetalleventa)) {
                    Medicamento oldIdMedicamentoOfDetalleventaListNewDetalleventa = detalleventaListNewDetalleventa.getIdMedicamento();
                    detalleventaListNewDetalleventa.setIdMedicamento(medicamento);
                    detalleventaListNewDetalleventa = em.merge(detalleventaListNewDetalleventa);
                    if (oldIdMedicamentoOfDetalleventaListNewDetalleventa != null && !oldIdMedicamentoOfDetalleventaListNewDetalleventa.equals(medicamento)) {
                        oldIdMedicamentoOfDetalleventaListNewDetalleventa.getDetalleventaList().remove(detalleventaListNewDetalleventa);
                        oldIdMedicamentoOfDetalleventaListNewDetalleventa = em.merge(oldIdMedicamentoOfDetalleventaListNewDetalleventa);
                    }
                }
            }
            for (Detallecompra detallecompraListNewDetallecompra : detallecompraListNew) {
                if (!detallecompraListOld.contains(detallecompraListNewDetallecompra)) {
                    Medicamento oldIdMedicamentoOfDetallecompraListNewDetallecompra = detallecompraListNewDetallecompra.getIdMedicamento();
                    detallecompraListNewDetallecompra.setIdMedicamento(medicamento);
                    detallecompraListNewDetallecompra = em.merge(detallecompraListNewDetallecompra);
                    if (oldIdMedicamentoOfDetallecompraListNewDetallecompra != null && !oldIdMedicamentoOfDetallecompraListNewDetallecompra.equals(medicamento)) {
                        oldIdMedicamentoOfDetallecompraListNewDetallecompra.getDetallecompraList().remove(detallecompraListNewDetallecompra);
                        oldIdMedicamentoOfDetallecompraListNewDetallecompra = em.merge(oldIdMedicamentoOfDetallecompraListNewDetallecompra);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = medicamento.getIdMedicamento();
                if (findMedicamento(id) == null) {
                    throw new NonexistentEntityException("The medicamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Medicamento medicamento;
            try {
                medicamento = em.getReference(Medicamento.class, id);
                medicamento.getIdMedicamento();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The medicamento with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Oferta> ofertaListOrphanCheck = medicamento.getOfertaList();
            for (Oferta ofertaListOrphanCheckOferta : ofertaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Medicamento (" + medicamento + ") cannot be destroyed since the Oferta " + ofertaListOrphanCheckOferta + " in its ofertaList field has a non-nullable idMedicamento field.");
            }
            List<Detalleventa> detalleventaListOrphanCheck = medicamento.getDetalleventaList();
            for (Detalleventa detalleventaListOrphanCheckDetalleventa : detalleventaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Medicamento (" + medicamento + ") cannot be destroyed since the Detalleventa " + detalleventaListOrphanCheckDetalleventa + " in its detalleventaList field has a non-nullable idMedicamento field.");
            }
            List<Detallecompra> detallecompraListOrphanCheck = medicamento.getDetallecompraList();
            for (Detallecompra detallecompraListOrphanCheckDetallecompra : detallecompraListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Medicamento (" + medicamento + ") cannot be destroyed since the Detallecompra " + detallecompraListOrphanCheckDetallecompra + " in its detallecompraList field has a non-nullable idMedicamento field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Presentacionmedicamento idPresentacionMedicamento = medicamento.getIdPresentacionMedicamento();
            if (idPresentacionMedicamento != null) {
                idPresentacionMedicamento.getMedicamentoList().remove(medicamento);
                idPresentacionMedicamento = em.merge(idPresentacionMedicamento);
            }
            Proveedor idProveedor = medicamento.getIdProveedor();
            if (idProveedor != null) {
                idProveedor.getMedicamentoList().remove(medicamento);
                idProveedor = em.merge(idProveedor);
            }
            Tipomedicamento idTipoMedicamento = medicamento.getIdTipoMedicamento();
            if (idTipoMedicamento != null) {
                idTipoMedicamento.getMedicamentoList().remove(medicamento);
                idTipoMedicamento = em.merge(idTipoMedicamento);
            }
            em.remove(medicamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Medicamento> findMedicamentoEntities() {
        return findMedicamentoEntities(true, -1, -1);
    }

    public List<Medicamento> findMedicamentoEntities(int maxResults, int firstResult) {
        return findMedicamentoEntities(false, maxResults, firstResult);
    }

    private List<Medicamento> findMedicamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Medicamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Medicamento findMedicamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Medicamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getMedicamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Medicamento> rt = cq.from(Medicamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
