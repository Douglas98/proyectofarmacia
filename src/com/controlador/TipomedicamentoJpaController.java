/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Medicamento;
import com.entidades.Tipomedicamento;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class TipomedicamentoJpaController implements Serializable {

    public TipomedicamentoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public TipomedicamentoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tipomedicamento tipomedicamento) throws PreexistingEntityException, Exception {
        if (tipomedicamento.getMedicamentoList() == null) {
            tipomedicamento.setMedicamentoList(new ArrayList<Medicamento>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Medicamento> attachedMedicamentoList = new ArrayList<Medicamento>();
            for (Medicamento medicamentoListMedicamentoToAttach : tipomedicamento.getMedicamentoList()) {
                medicamentoListMedicamentoToAttach = em.getReference(medicamentoListMedicamentoToAttach.getClass(), medicamentoListMedicamentoToAttach.getIdMedicamento());
                attachedMedicamentoList.add(medicamentoListMedicamentoToAttach);
            }
            tipomedicamento.setMedicamentoList(attachedMedicamentoList);
            em.persist(tipomedicamento);
            for (Medicamento medicamentoListMedicamento : tipomedicamento.getMedicamentoList()) {
                Tipomedicamento oldIdTipoMedicamentoOfMedicamentoListMedicamento = medicamentoListMedicamento.getIdTipoMedicamento();
                medicamentoListMedicamento.setIdTipoMedicamento(tipomedicamento);
                medicamentoListMedicamento = em.merge(medicamentoListMedicamento);
                if (oldIdTipoMedicamentoOfMedicamentoListMedicamento != null) {
                    oldIdTipoMedicamentoOfMedicamentoListMedicamento.getMedicamentoList().remove(medicamentoListMedicamento);
                    oldIdTipoMedicamentoOfMedicamentoListMedicamento = em.merge(oldIdTipoMedicamentoOfMedicamentoListMedicamento);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTipomedicamento(tipomedicamento.getIdTipoMedicamento()) != null) {
                throw new PreexistingEntityException("Tipomedicamento " + tipomedicamento + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tipomedicamento tipomedicamento) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tipomedicamento persistentTipomedicamento = em.find(Tipomedicamento.class, tipomedicamento.getIdTipoMedicamento());
            List<Medicamento> medicamentoListOld = persistentTipomedicamento.getMedicamentoList();
            List<Medicamento> medicamentoListNew = tipomedicamento.getMedicamentoList();
            List<String> illegalOrphanMessages = null;
            for (Medicamento medicamentoListOldMedicamento : medicamentoListOld) {
                if (!medicamentoListNew.contains(medicamentoListOldMedicamento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Medicamento " + medicamentoListOldMedicamento + " since its idTipoMedicamento field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Medicamento> attachedMedicamentoListNew = new ArrayList<Medicamento>();
            for (Medicamento medicamentoListNewMedicamentoToAttach : medicamentoListNew) {
                medicamentoListNewMedicamentoToAttach = em.getReference(medicamentoListNewMedicamentoToAttach.getClass(), medicamentoListNewMedicamentoToAttach.getIdMedicamento());
                attachedMedicamentoListNew.add(medicamentoListNewMedicamentoToAttach);
            }
            medicamentoListNew = attachedMedicamentoListNew;
            tipomedicamento.setMedicamentoList(medicamentoListNew);
            tipomedicamento = em.merge(tipomedicamento);
            for (Medicamento medicamentoListNewMedicamento : medicamentoListNew) {
                if (!medicamentoListOld.contains(medicamentoListNewMedicamento)) {
                    Tipomedicamento oldIdTipoMedicamentoOfMedicamentoListNewMedicamento = medicamentoListNewMedicamento.getIdTipoMedicamento();
                    medicamentoListNewMedicamento.setIdTipoMedicamento(tipomedicamento);
                    medicamentoListNewMedicamento = em.merge(medicamentoListNewMedicamento);
                    if (oldIdTipoMedicamentoOfMedicamentoListNewMedicamento != null && !oldIdTipoMedicamentoOfMedicamentoListNewMedicamento.equals(tipomedicamento)) {
                        oldIdTipoMedicamentoOfMedicamentoListNewMedicamento.getMedicamentoList().remove(medicamentoListNewMedicamento);
                        oldIdTipoMedicamentoOfMedicamentoListNewMedicamento = em.merge(oldIdTipoMedicamentoOfMedicamentoListNewMedicamento);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipomedicamento.getIdTipoMedicamento();
                if (findTipomedicamento(id) == null) {
                    throw new NonexistentEntityException("The tipomedicamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tipomedicamento tipomedicamento;
            try {
                tipomedicamento = em.getReference(Tipomedicamento.class, id);
                tipomedicamento.getIdTipoMedicamento();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipomedicamento with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Medicamento> medicamentoListOrphanCheck = tipomedicamento.getMedicamentoList();
            for (Medicamento medicamentoListOrphanCheckMedicamento : medicamentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Tipomedicamento (" + tipomedicamento + ") cannot be destroyed since the Medicamento " + medicamentoListOrphanCheckMedicamento + " in its medicamentoList field has a non-nullable idTipoMedicamento field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipomedicamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tipomedicamento> findTipomedicamentoEntities() {
        return findTipomedicamentoEntities(true, -1, -1);
    }

    public List<Tipomedicamento> findTipomedicamentoEntities(int maxResults, int firstResult) {
        return findTipomedicamentoEntities(false, maxResults, firstResult);
    }

    private List<Tipomedicamento> findTipomedicamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tipomedicamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tipomedicamento findTipomedicamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tipomedicamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipomedicamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tipomedicamento> rt = cq.from(Tipomedicamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
