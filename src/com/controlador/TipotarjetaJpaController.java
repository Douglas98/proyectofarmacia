/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Tarjeta;
import com.entidades.Tipotarjeta;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class TipotarjetaJpaController implements Serializable {

    public TipotarjetaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public TipotarjetaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tipotarjeta tipotarjeta) throws PreexistingEntityException, Exception {
        if (tipotarjeta.getTarjetaList() == null) {
            tipotarjeta.setTarjetaList(new ArrayList<Tarjeta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Tarjeta> attachedTarjetaList = new ArrayList<Tarjeta>();
            for (Tarjeta tarjetaListTarjetaToAttach : tipotarjeta.getTarjetaList()) {
                tarjetaListTarjetaToAttach = em.getReference(tarjetaListTarjetaToAttach.getClass(), tarjetaListTarjetaToAttach.getIdtarjeta());
                attachedTarjetaList.add(tarjetaListTarjetaToAttach);
            }
            tipotarjeta.setTarjetaList(attachedTarjetaList);
            em.persist(tipotarjeta);
            for (Tarjeta tarjetaListTarjeta : tipotarjeta.getTarjetaList()) {
                Tipotarjeta oldIdTipotarjetaOfTarjetaListTarjeta = tarjetaListTarjeta.getIdTipotarjeta();
                tarjetaListTarjeta.setIdTipotarjeta(tipotarjeta);
                tarjetaListTarjeta = em.merge(tarjetaListTarjeta);
                if (oldIdTipotarjetaOfTarjetaListTarjeta != null) {
                    oldIdTipotarjetaOfTarjetaListTarjeta.getTarjetaList().remove(tarjetaListTarjeta);
                    oldIdTipotarjetaOfTarjetaListTarjeta = em.merge(oldIdTipotarjetaOfTarjetaListTarjeta);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTipotarjeta(tipotarjeta.getIdTipoTarjeta()) != null) {
                throw new PreexistingEntityException("Tipotarjeta " + tipotarjeta + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tipotarjeta tipotarjeta) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tipotarjeta persistentTipotarjeta = em.find(Tipotarjeta.class, tipotarjeta.getIdTipoTarjeta());
            List<Tarjeta> tarjetaListOld = persistentTipotarjeta.getTarjetaList();
            List<Tarjeta> tarjetaListNew = tipotarjeta.getTarjetaList();
            List<String> illegalOrphanMessages = null;
            for (Tarjeta tarjetaListOldTarjeta : tarjetaListOld) {
                if (!tarjetaListNew.contains(tarjetaListOldTarjeta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Tarjeta " + tarjetaListOldTarjeta + " since its idTipotarjeta field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Tarjeta> attachedTarjetaListNew = new ArrayList<Tarjeta>();
            for (Tarjeta tarjetaListNewTarjetaToAttach : tarjetaListNew) {
                tarjetaListNewTarjetaToAttach = em.getReference(tarjetaListNewTarjetaToAttach.getClass(), tarjetaListNewTarjetaToAttach.getIdtarjeta());
                attachedTarjetaListNew.add(tarjetaListNewTarjetaToAttach);
            }
            tarjetaListNew = attachedTarjetaListNew;
            tipotarjeta.setTarjetaList(tarjetaListNew);
            tipotarjeta = em.merge(tipotarjeta);
            for (Tarjeta tarjetaListNewTarjeta : tarjetaListNew) {
                if (!tarjetaListOld.contains(tarjetaListNewTarjeta)) {
                    Tipotarjeta oldIdTipotarjetaOfTarjetaListNewTarjeta = tarjetaListNewTarjeta.getIdTipotarjeta();
                    tarjetaListNewTarjeta.setIdTipotarjeta(tipotarjeta);
                    tarjetaListNewTarjeta = em.merge(tarjetaListNewTarjeta);
                    if (oldIdTipotarjetaOfTarjetaListNewTarjeta != null && !oldIdTipotarjetaOfTarjetaListNewTarjeta.equals(tipotarjeta)) {
                        oldIdTipotarjetaOfTarjetaListNewTarjeta.getTarjetaList().remove(tarjetaListNewTarjeta);
                        oldIdTipotarjetaOfTarjetaListNewTarjeta = em.merge(oldIdTipotarjetaOfTarjetaListNewTarjeta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipotarjeta.getIdTipoTarjeta();
                if (findTipotarjeta(id) == null) {
                    throw new NonexistentEntityException("The tipotarjeta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tipotarjeta tipotarjeta;
            try {
                tipotarjeta = em.getReference(Tipotarjeta.class, id);
                tipotarjeta.getIdTipoTarjeta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipotarjeta with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Tarjeta> tarjetaListOrphanCheck = tipotarjeta.getTarjetaList();
            for (Tarjeta tarjetaListOrphanCheckTarjeta : tarjetaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Tipotarjeta (" + tipotarjeta + ") cannot be destroyed since the Tarjeta " + tarjetaListOrphanCheckTarjeta + " in its tarjetaList field has a non-nullable idTipotarjeta field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipotarjeta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tipotarjeta> findTipotarjetaEntities() {
        return findTipotarjetaEntities(true, -1, -1);
    }

    public List<Tipotarjeta> findTipotarjetaEntities(int maxResults, int firstResult) {
        return findTipotarjetaEntities(false, maxResults, firstResult);
    }

    private List<Tipotarjeta> findTipotarjetaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tipotarjeta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tipotarjeta findTipotarjeta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tipotarjeta.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipotarjetaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tipotarjeta> rt = cq.from(Tipotarjeta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
