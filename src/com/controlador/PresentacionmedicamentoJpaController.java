/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Medicamento;
import com.entidades.Presentacionmedicamento;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class PresentacionmedicamentoJpaController implements Serializable {

    public PresentacionmedicamentoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
     public PresentacionmedicamentoJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Presentacionmedicamento presentacionmedicamento) throws PreexistingEntityException, Exception {
        if (presentacionmedicamento.getMedicamentoList() == null) {
            presentacionmedicamento.setMedicamentoList(new ArrayList<Medicamento>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Medicamento> attachedMedicamentoList = new ArrayList<Medicamento>();
            for (Medicamento medicamentoListMedicamentoToAttach : presentacionmedicamento.getMedicamentoList()) {
                medicamentoListMedicamentoToAttach = em.getReference(medicamentoListMedicamentoToAttach.getClass(), medicamentoListMedicamentoToAttach.getIdMedicamento());
                attachedMedicamentoList.add(medicamentoListMedicamentoToAttach);
            }
            presentacionmedicamento.setMedicamentoList(attachedMedicamentoList);
            em.persist(presentacionmedicamento);
            for (Medicamento medicamentoListMedicamento : presentacionmedicamento.getMedicamentoList()) {
                Presentacionmedicamento oldIdPresentacionMedicamentoOfMedicamentoListMedicamento = medicamentoListMedicamento.getIdPresentacionMedicamento();
                medicamentoListMedicamento.setIdPresentacionMedicamento(presentacionmedicamento);
                medicamentoListMedicamento = em.merge(medicamentoListMedicamento);
                if (oldIdPresentacionMedicamentoOfMedicamentoListMedicamento != null) {
                    oldIdPresentacionMedicamentoOfMedicamentoListMedicamento.getMedicamentoList().remove(medicamentoListMedicamento);
                    oldIdPresentacionMedicamentoOfMedicamentoListMedicamento = em.merge(oldIdPresentacionMedicamentoOfMedicamentoListMedicamento);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPresentacionmedicamento(presentacionmedicamento.getIdPresentacionMedicamento()) != null) {
                throw new PreexistingEntityException("Presentacionmedicamento " + presentacionmedicamento + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Presentacionmedicamento presentacionmedicamento) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Presentacionmedicamento persistentPresentacionmedicamento = em.find(Presentacionmedicamento.class, presentacionmedicamento.getIdPresentacionMedicamento());
            List<Medicamento> medicamentoListOld = persistentPresentacionmedicamento.getMedicamentoList();
            List<Medicamento> medicamentoListNew = presentacionmedicamento.getMedicamentoList();
            List<String> illegalOrphanMessages = null;
            for (Medicamento medicamentoListOldMedicamento : medicamentoListOld) {
                if (!medicamentoListNew.contains(medicamentoListOldMedicamento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Medicamento " + medicamentoListOldMedicamento + " since its idPresentacionMedicamento field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Medicamento> attachedMedicamentoListNew = new ArrayList<Medicamento>();
            for (Medicamento medicamentoListNewMedicamentoToAttach : medicamentoListNew) {
                medicamentoListNewMedicamentoToAttach = em.getReference(medicamentoListNewMedicamentoToAttach.getClass(), medicamentoListNewMedicamentoToAttach.getIdMedicamento());
                attachedMedicamentoListNew.add(medicamentoListNewMedicamentoToAttach);
            }
            medicamentoListNew = attachedMedicamentoListNew;
            presentacionmedicamento.setMedicamentoList(medicamentoListNew);
            presentacionmedicamento = em.merge(presentacionmedicamento);
            for (Medicamento medicamentoListNewMedicamento : medicamentoListNew) {
                if (!medicamentoListOld.contains(medicamentoListNewMedicamento)) {
                    Presentacionmedicamento oldIdPresentacionMedicamentoOfMedicamentoListNewMedicamento = medicamentoListNewMedicamento.getIdPresentacionMedicamento();
                    medicamentoListNewMedicamento.setIdPresentacionMedicamento(presentacionmedicamento);
                    medicamentoListNewMedicamento = em.merge(medicamentoListNewMedicamento);
                    if (oldIdPresentacionMedicamentoOfMedicamentoListNewMedicamento != null && !oldIdPresentacionMedicamentoOfMedicamentoListNewMedicamento.equals(presentacionmedicamento)) {
                        oldIdPresentacionMedicamentoOfMedicamentoListNewMedicamento.getMedicamentoList().remove(medicamentoListNewMedicamento);
                        oldIdPresentacionMedicamentoOfMedicamentoListNewMedicamento = em.merge(oldIdPresentacionMedicamentoOfMedicamentoListNewMedicamento);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = presentacionmedicamento.getIdPresentacionMedicamento();
                if (findPresentacionmedicamento(id) == null) {
                    throw new NonexistentEntityException("The presentacionmedicamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Presentacionmedicamento presentacionmedicamento;
            try {
                presentacionmedicamento = em.getReference(Presentacionmedicamento.class, id);
                presentacionmedicamento.getIdPresentacionMedicamento();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The presentacionmedicamento with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Medicamento> medicamentoListOrphanCheck = presentacionmedicamento.getMedicamentoList();
            for (Medicamento medicamentoListOrphanCheckMedicamento : medicamentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Presentacionmedicamento (" + presentacionmedicamento + ") cannot be destroyed since the Medicamento " + medicamentoListOrphanCheckMedicamento + " in its medicamentoList field has a non-nullable idPresentacionMedicamento field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(presentacionmedicamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Presentacionmedicamento> findPresentacionmedicamentoEntities() {
        return findPresentacionmedicamentoEntities(true, -1, -1);
    }

    public List<Presentacionmedicamento> findPresentacionmedicamentoEntities(int maxResults, int firstResult) {
        return findPresentacionmedicamentoEntities(false, maxResults, firstResult);
    }

    private List<Presentacionmedicamento> findPresentacionmedicamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Presentacionmedicamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Presentacionmedicamento findPresentacionmedicamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Presentacionmedicamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getPresentacionmedicamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Presentacionmedicamento> rt = cq.from(Presentacionmedicamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
