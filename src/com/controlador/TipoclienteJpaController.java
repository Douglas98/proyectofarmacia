/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Oferta;
import java.util.ArrayList;
import java.util.List;
import com.entidades.Cliente;
import com.entidades.Tipocliente;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class TipoclienteJpaController implements Serializable {

    public TipoclienteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public TipoclienteJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tipocliente tipocliente) throws PreexistingEntityException, Exception {
        if (tipocliente.getOfertaList() == null) {
            tipocliente.setOfertaList(new ArrayList<Oferta>());
        }
        if (tipocliente.getClienteList() == null) {
            tipocliente.setClienteList(new ArrayList<Cliente>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Oferta> attachedOfertaList = new ArrayList<Oferta>();
            for (Oferta ofertaListOfertaToAttach : tipocliente.getOfertaList()) {
                ofertaListOfertaToAttach = em.getReference(ofertaListOfertaToAttach.getClass(), ofertaListOfertaToAttach.getIdOferta());
                attachedOfertaList.add(ofertaListOfertaToAttach);
            }
            tipocliente.setOfertaList(attachedOfertaList);
            List<Cliente> attachedClienteList = new ArrayList<Cliente>();
            for (Cliente clienteListClienteToAttach : tipocliente.getClienteList()) {
                clienteListClienteToAttach = em.getReference(clienteListClienteToAttach.getClass(), clienteListClienteToAttach.getIdCliente());
                attachedClienteList.add(clienteListClienteToAttach);
            }
            tipocliente.setClienteList(attachedClienteList);
            em.persist(tipocliente);
            for (Oferta ofertaListOferta : tipocliente.getOfertaList()) {
                Tipocliente oldIdTipoClienteOfOfertaListOferta = ofertaListOferta.getIdTipoCliente();
                ofertaListOferta.setIdTipoCliente(tipocliente);
                ofertaListOferta = em.merge(ofertaListOferta);
                if (oldIdTipoClienteOfOfertaListOferta != null) {
                    oldIdTipoClienteOfOfertaListOferta.getOfertaList().remove(ofertaListOferta);
                    oldIdTipoClienteOfOfertaListOferta = em.merge(oldIdTipoClienteOfOfertaListOferta);
                }
            }
            for (Cliente clienteListCliente : tipocliente.getClienteList()) {
                Tipocliente oldIdTipoClienteOfClienteListCliente = clienteListCliente.getIdTipoCliente();
                clienteListCliente.setIdTipoCliente(tipocliente);
                clienteListCliente = em.merge(clienteListCliente);
                if (oldIdTipoClienteOfClienteListCliente != null) {
                    oldIdTipoClienteOfClienteListCliente.getClienteList().remove(clienteListCliente);
                    oldIdTipoClienteOfClienteListCliente = em.merge(oldIdTipoClienteOfClienteListCliente);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTipocliente(tipocliente.getIdTipoCliente()) != null) {
                throw new PreexistingEntityException("Tipocliente " + tipocliente + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tipocliente tipocliente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tipocliente persistentTipocliente = em.find(Tipocliente.class, tipocliente.getIdTipoCliente());
            List<Oferta> ofertaListOld = persistentTipocliente.getOfertaList();
            List<Oferta> ofertaListNew = tipocliente.getOfertaList();
            List<Cliente> clienteListOld = persistentTipocliente.getClienteList();
            List<Cliente> clienteListNew = tipocliente.getClienteList();
            List<String> illegalOrphanMessages = null;
            for (Oferta ofertaListOldOferta : ofertaListOld) {
                if (!ofertaListNew.contains(ofertaListOldOferta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Oferta " + ofertaListOldOferta + " since its idTipoCliente field is not nullable.");
                }
            }
            for (Cliente clienteListOldCliente : clienteListOld) {
                if (!clienteListNew.contains(clienteListOldCliente)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cliente " + clienteListOldCliente + " since its idTipoCliente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Oferta> attachedOfertaListNew = new ArrayList<Oferta>();
            for (Oferta ofertaListNewOfertaToAttach : ofertaListNew) {
                ofertaListNewOfertaToAttach = em.getReference(ofertaListNewOfertaToAttach.getClass(), ofertaListNewOfertaToAttach.getIdOferta());
                attachedOfertaListNew.add(ofertaListNewOfertaToAttach);
            }
            ofertaListNew = attachedOfertaListNew;
            tipocliente.setOfertaList(ofertaListNew);
            List<Cliente> attachedClienteListNew = new ArrayList<Cliente>();
            for (Cliente clienteListNewClienteToAttach : clienteListNew) {
                clienteListNewClienteToAttach = em.getReference(clienteListNewClienteToAttach.getClass(), clienteListNewClienteToAttach.getIdCliente());
                attachedClienteListNew.add(clienteListNewClienteToAttach);
            }
            clienteListNew = attachedClienteListNew;
            tipocliente.setClienteList(clienteListNew);
            tipocliente = em.merge(tipocliente);
            for (Oferta ofertaListNewOferta : ofertaListNew) {
                if (!ofertaListOld.contains(ofertaListNewOferta)) {
                    Tipocliente oldIdTipoClienteOfOfertaListNewOferta = ofertaListNewOferta.getIdTipoCliente();
                    ofertaListNewOferta.setIdTipoCliente(tipocliente);
                    ofertaListNewOferta = em.merge(ofertaListNewOferta);
                    if (oldIdTipoClienteOfOfertaListNewOferta != null && !oldIdTipoClienteOfOfertaListNewOferta.equals(tipocliente)) {
                        oldIdTipoClienteOfOfertaListNewOferta.getOfertaList().remove(ofertaListNewOferta);
                        oldIdTipoClienteOfOfertaListNewOferta = em.merge(oldIdTipoClienteOfOfertaListNewOferta);
                    }
                }
            }
            for (Cliente clienteListNewCliente : clienteListNew) {
                if (!clienteListOld.contains(clienteListNewCliente)) {
                    Tipocliente oldIdTipoClienteOfClienteListNewCliente = clienteListNewCliente.getIdTipoCliente();
                    clienteListNewCliente.setIdTipoCliente(tipocliente);
                    clienteListNewCliente = em.merge(clienteListNewCliente);
                    if (oldIdTipoClienteOfClienteListNewCliente != null && !oldIdTipoClienteOfClienteListNewCliente.equals(tipocliente)) {
                        oldIdTipoClienteOfClienteListNewCliente.getClienteList().remove(clienteListNewCliente);
                        oldIdTipoClienteOfClienteListNewCliente = em.merge(oldIdTipoClienteOfClienteListNewCliente);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipocliente.getIdTipoCliente();
                if (findTipocliente(id) == null) {
                    throw new NonexistentEntityException("The tipocliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tipocliente tipocliente;
            try {
                tipocliente = em.getReference(Tipocliente.class, id);
                tipocliente.getIdTipoCliente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipocliente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Oferta> ofertaListOrphanCheck = tipocliente.getOfertaList();
            for (Oferta ofertaListOrphanCheckOferta : ofertaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Tipocliente (" + tipocliente + ") cannot be destroyed since the Oferta " + ofertaListOrphanCheckOferta + " in its ofertaList field has a non-nullable idTipoCliente field.");
            }
            List<Cliente> clienteListOrphanCheck = tipocliente.getClienteList();
            for (Cliente clienteListOrphanCheckCliente : clienteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Tipocliente (" + tipocliente + ") cannot be destroyed since the Cliente " + clienteListOrphanCheckCliente + " in its clienteList field has a non-nullable idTipoCliente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipocliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tipocliente> findTipoclienteEntities() {
        return findTipoclienteEntities(true, -1, -1);
    }

    public List<Tipocliente> findTipoclienteEntities(int maxResults, int firstResult) {
        return findTipoclienteEntities(false, maxResults, firstResult);
    }

    private List<Tipocliente> findTipoclienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tipocliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tipocliente findTipocliente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tipocliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoclienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tipocliente> rt = cq.from(Tipocliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
