/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Cliente;
import com.entidades.Tarjeta;
import com.entidades.Tipotarjeta;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class TarjetaJpaController implements Serializable {

    public TarjetaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
     public TarjetaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tarjeta tarjeta) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente idCliente = tarjeta.getIdCliente();
            if (idCliente != null) {
                idCliente = em.getReference(idCliente.getClass(), idCliente.getIdCliente());
                tarjeta.setIdCliente(idCliente);
            }
            Tipotarjeta idTipotarjeta = tarjeta.getIdTipotarjeta();
            if (idTipotarjeta != null) {
                idTipotarjeta = em.getReference(idTipotarjeta.getClass(), idTipotarjeta.getIdTipoTarjeta());
                tarjeta.setIdTipotarjeta(idTipotarjeta);
            }
            em.persist(tarjeta);
            if (idCliente != null) {
                idCliente.getTarjetaList().add(tarjeta);
                idCliente = em.merge(idCliente);
            }
            if (idTipotarjeta != null) {
                idTipotarjeta.getTarjetaList().add(tarjeta);
                idTipotarjeta = em.merge(idTipotarjeta);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTarjeta(tarjeta.getIdtarjeta()) != null) {
                throw new PreexistingEntityException("Tarjeta " + tarjeta + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tarjeta tarjeta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tarjeta persistentTarjeta = em.find(Tarjeta.class, tarjeta.getIdtarjeta());
            Cliente idClienteOld = persistentTarjeta.getIdCliente();
            Cliente idClienteNew = tarjeta.getIdCliente();
            Tipotarjeta idTipotarjetaOld = persistentTarjeta.getIdTipotarjeta();
            Tipotarjeta idTipotarjetaNew = tarjeta.getIdTipotarjeta();
            if (idClienteNew != null) {
                idClienteNew = em.getReference(idClienteNew.getClass(), idClienteNew.getIdCliente());
                tarjeta.setIdCliente(idClienteNew);
            }
            if (idTipotarjetaNew != null) {
                idTipotarjetaNew = em.getReference(idTipotarjetaNew.getClass(), idTipotarjetaNew.getIdTipoTarjeta());
                tarjeta.setIdTipotarjeta(idTipotarjetaNew);
            }
            tarjeta = em.merge(tarjeta);
            if (idClienteOld != null && !idClienteOld.equals(idClienteNew)) {
                idClienteOld.getTarjetaList().remove(tarjeta);
                idClienteOld = em.merge(idClienteOld);
            }
            if (idClienteNew != null && !idClienteNew.equals(idClienteOld)) {
                idClienteNew.getTarjetaList().add(tarjeta);
                idClienteNew = em.merge(idClienteNew);
            }
            if (idTipotarjetaOld != null && !idTipotarjetaOld.equals(idTipotarjetaNew)) {
                idTipotarjetaOld.getTarjetaList().remove(tarjeta);
                idTipotarjetaOld = em.merge(idTipotarjetaOld);
            }
            if (idTipotarjetaNew != null && !idTipotarjetaNew.equals(idTipotarjetaOld)) {
                idTipotarjetaNew.getTarjetaList().add(tarjeta);
                idTipotarjetaNew = em.merge(idTipotarjetaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tarjeta.getIdtarjeta();
                if (findTarjeta(id) == null) {
                    throw new NonexistentEntityException("The tarjeta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tarjeta tarjeta;
            try {
                tarjeta = em.getReference(Tarjeta.class, id);
                tarjeta.getIdtarjeta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tarjeta with id " + id + " no longer exists.", enfe);
            }
            Cliente idCliente = tarjeta.getIdCliente();
            if (idCliente != null) {
                idCliente.getTarjetaList().remove(tarjeta);
                idCliente = em.merge(idCliente);
            }
            Tipotarjeta idTipotarjeta = tarjeta.getIdTipotarjeta();
            if (idTipotarjeta != null) {
                idTipotarjeta.getTarjetaList().remove(tarjeta);
                idTipotarjeta = em.merge(idTipotarjeta);
            }
            em.remove(tarjeta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tarjeta> findTarjetaEntities() {
        return findTarjetaEntities(true, -1, -1);
    }

    public List<Tarjeta> findTarjetaEntities(int maxResults, int firstResult) {
        return findTarjetaEntities(false, maxResults, firstResult);
    }

    private List<Tarjeta> findTarjetaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tarjeta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tarjeta findTarjeta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tarjeta.class, id);
        } finally {
            em.close();
        }
    }

    public int getTarjetaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tarjeta> rt = cq.from(Tarjeta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
