/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Municipios;
import com.entidades.Compra;
import java.util.ArrayList;
import java.util.List;
import com.entidades.Vendedor;
import com.entidades.Oferta;
import com.entidades.Sucursal;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class SucursalJpaController implements Serializable {

    public SucursalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public SucursalJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sucursal sucursal) throws PreexistingEntityException, Exception {
        if (sucursal.getCompraList() == null) {
            sucursal.setCompraList(new ArrayList<Compra>());
        }
        if (sucursal.getVendedorList() == null) {
            sucursal.setVendedorList(new ArrayList<Vendedor>());
        }
        if (sucursal.getOfertaList() == null) {
            sucursal.setOfertaList(new ArrayList<Oferta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipios idMunicipio = sucursal.getIdMunicipio();
            if (idMunicipio != null) {
                idMunicipio = em.getReference(idMunicipio.getClass(), idMunicipio.getIdMunicipio());
                sucursal.setIdMunicipio(idMunicipio);
            }
            List<Compra> attachedCompraList = new ArrayList<Compra>();
            for (Compra compraListCompraToAttach : sucursal.getCompraList()) {
                compraListCompraToAttach = em.getReference(compraListCompraToAttach.getClass(), compraListCompraToAttach.getIdCompra());
                attachedCompraList.add(compraListCompraToAttach);
            }
            sucursal.setCompraList(attachedCompraList);
            List<Vendedor> attachedVendedorList = new ArrayList<Vendedor>();
            for (Vendedor vendedorListVendedorToAttach : sucursal.getVendedorList()) {
                vendedorListVendedorToAttach = em.getReference(vendedorListVendedorToAttach.getClass(), vendedorListVendedorToAttach.getIdVendedor());
                attachedVendedorList.add(vendedorListVendedorToAttach);
            }
            sucursal.setVendedorList(attachedVendedorList);
            List<Oferta> attachedOfertaList = new ArrayList<Oferta>();
            for (Oferta ofertaListOfertaToAttach : sucursal.getOfertaList()) {
                ofertaListOfertaToAttach = em.getReference(ofertaListOfertaToAttach.getClass(), ofertaListOfertaToAttach.getIdOferta());
                attachedOfertaList.add(ofertaListOfertaToAttach);
            }
            sucursal.setOfertaList(attachedOfertaList);
            em.persist(sucursal);
            if (idMunicipio != null) {
                idMunicipio.getSucursalList().add(sucursal);
                idMunicipio = em.merge(idMunicipio);
            }
            for (Compra compraListCompra : sucursal.getCompraList()) {
                Sucursal oldIdSucursalOfCompraListCompra = compraListCompra.getIdSucursal();
                compraListCompra.setIdSucursal(sucursal);
                compraListCompra = em.merge(compraListCompra);
                if (oldIdSucursalOfCompraListCompra != null) {
                    oldIdSucursalOfCompraListCompra.getCompraList().remove(compraListCompra);
                    oldIdSucursalOfCompraListCompra = em.merge(oldIdSucursalOfCompraListCompra);
                }
            }
            for (Vendedor vendedorListVendedor : sucursal.getVendedorList()) {
                Sucursal oldIdSucursalOfVendedorListVendedor = vendedorListVendedor.getIdSucursal();
                vendedorListVendedor.setIdSucursal(sucursal);
                vendedorListVendedor = em.merge(vendedorListVendedor);
                if (oldIdSucursalOfVendedorListVendedor != null) {
                    oldIdSucursalOfVendedorListVendedor.getVendedorList().remove(vendedorListVendedor);
                    oldIdSucursalOfVendedorListVendedor = em.merge(oldIdSucursalOfVendedorListVendedor);
                }
            }
            for (Oferta ofertaListOferta : sucursal.getOfertaList()) {
                Sucursal oldIdSucursalOfOfertaListOferta = ofertaListOferta.getIdSucursal();
                ofertaListOferta.setIdSucursal(sucursal);
                ofertaListOferta = em.merge(ofertaListOferta);
                if (oldIdSucursalOfOfertaListOferta != null) {
                    oldIdSucursalOfOfertaListOferta.getOfertaList().remove(ofertaListOferta);
                    oldIdSucursalOfOfertaListOferta = em.merge(oldIdSucursalOfOfertaListOferta);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSucursal(sucursal.getIdSucursal()) != null) {
                throw new PreexistingEntityException("Sucursal " + sucursal + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sucursal sucursal) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sucursal persistentSucursal = em.find(Sucursal.class, sucursal.getIdSucursal());
            Municipios idMunicipioOld = persistentSucursal.getIdMunicipio();
            Municipios idMunicipioNew = sucursal.getIdMunicipio();
            List<Compra> compraListOld = persistentSucursal.getCompraList();
            List<Compra> compraListNew = sucursal.getCompraList();
            List<Vendedor> vendedorListOld = persistentSucursal.getVendedorList();
            List<Vendedor> vendedorListNew = sucursal.getVendedorList();
            List<Oferta> ofertaListOld = persistentSucursal.getOfertaList();
            List<Oferta> ofertaListNew = sucursal.getOfertaList();
            List<String> illegalOrphanMessages = null;
            for (Compra compraListOldCompra : compraListOld) {
                if (!compraListNew.contains(compraListOldCompra)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Compra " + compraListOldCompra + " since its idSucursal field is not nullable.");
                }
            }
            for (Vendedor vendedorListOldVendedor : vendedorListOld) {
                if (!vendedorListNew.contains(vendedorListOldVendedor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Vendedor " + vendedorListOldVendedor + " since its idSucursal field is not nullable.");
                }
            }
            for (Oferta ofertaListOldOferta : ofertaListOld) {
                if (!ofertaListNew.contains(ofertaListOldOferta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Oferta " + ofertaListOldOferta + " since its idSucursal field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idMunicipioNew != null) {
                idMunicipioNew = em.getReference(idMunicipioNew.getClass(), idMunicipioNew.getIdMunicipio());
                sucursal.setIdMunicipio(idMunicipioNew);
            }
            List<Compra> attachedCompraListNew = new ArrayList<Compra>();
            for (Compra compraListNewCompraToAttach : compraListNew) {
                compraListNewCompraToAttach = em.getReference(compraListNewCompraToAttach.getClass(), compraListNewCompraToAttach.getIdCompra());
                attachedCompraListNew.add(compraListNewCompraToAttach);
            }
            compraListNew = attachedCompraListNew;
            sucursal.setCompraList(compraListNew);
            List<Vendedor> attachedVendedorListNew = new ArrayList<Vendedor>();
            for (Vendedor vendedorListNewVendedorToAttach : vendedorListNew) {
                vendedorListNewVendedorToAttach = em.getReference(vendedorListNewVendedorToAttach.getClass(), vendedorListNewVendedorToAttach.getIdVendedor());
                attachedVendedorListNew.add(vendedorListNewVendedorToAttach);
            }
            vendedorListNew = attachedVendedorListNew;
            sucursal.setVendedorList(vendedorListNew);
            List<Oferta> attachedOfertaListNew = new ArrayList<Oferta>();
            for (Oferta ofertaListNewOfertaToAttach : ofertaListNew) {
                ofertaListNewOfertaToAttach = em.getReference(ofertaListNewOfertaToAttach.getClass(), ofertaListNewOfertaToAttach.getIdOferta());
                attachedOfertaListNew.add(ofertaListNewOfertaToAttach);
            }
            ofertaListNew = attachedOfertaListNew;
            sucursal.setOfertaList(ofertaListNew);
            sucursal = em.merge(sucursal);
            if (idMunicipioOld != null && !idMunicipioOld.equals(idMunicipioNew)) {
                idMunicipioOld.getSucursalList().remove(sucursal);
                idMunicipioOld = em.merge(idMunicipioOld);
            }
            if (idMunicipioNew != null && !idMunicipioNew.equals(idMunicipioOld)) {
                idMunicipioNew.getSucursalList().add(sucursal);
                idMunicipioNew = em.merge(idMunicipioNew);
            }
            for (Compra compraListNewCompra : compraListNew) {
                if (!compraListOld.contains(compraListNewCompra)) {
                    Sucursal oldIdSucursalOfCompraListNewCompra = compraListNewCompra.getIdSucursal();
                    compraListNewCompra.setIdSucursal(sucursal);
                    compraListNewCompra = em.merge(compraListNewCompra);
                    if (oldIdSucursalOfCompraListNewCompra != null && !oldIdSucursalOfCompraListNewCompra.equals(sucursal)) {
                        oldIdSucursalOfCompraListNewCompra.getCompraList().remove(compraListNewCompra);
                        oldIdSucursalOfCompraListNewCompra = em.merge(oldIdSucursalOfCompraListNewCompra);
                    }
                }
            }
            for (Vendedor vendedorListNewVendedor : vendedorListNew) {
                if (!vendedorListOld.contains(vendedorListNewVendedor)) {
                    Sucursal oldIdSucursalOfVendedorListNewVendedor = vendedorListNewVendedor.getIdSucursal();
                    vendedorListNewVendedor.setIdSucursal(sucursal);
                    vendedorListNewVendedor = em.merge(vendedorListNewVendedor);
                    if (oldIdSucursalOfVendedorListNewVendedor != null && !oldIdSucursalOfVendedorListNewVendedor.equals(sucursal)) {
                        oldIdSucursalOfVendedorListNewVendedor.getVendedorList().remove(vendedorListNewVendedor);
                        oldIdSucursalOfVendedorListNewVendedor = em.merge(oldIdSucursalOfVendedorListNewVendedor);
                    }
                }
            }
            for (Oferta ofertaListNewOferta : ofertaListNew) {
                if (!ofertaListOld.contains(ofertaListNewOferta)) {
                    Sucursal oldIdSucursalOfOfertaListNewOferta = ofertaListNewOferta.getIdSucursal();
                    ofertaListNewOferta.setIdSucursal(sucursal);
                    ofertaListNewOferta = em.merge(ofertaListNewOferta);
                    if (oldIdSucursalOfOfertaListNewOferta != null && !oldIdSucursalOfOfertaListNewOferta.equals(sucursal)) {
                        oldIdSucursalOfOfertaListNewOferta.getOfertaList().remove(ofertaListNewOferta);
                        oldIdSucursalOfOfertaListNewOferta = em.merge(oldIdSucursalOfOfertaListNewOferta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sucursal.getIdSucursal();
                if (findSucursal(id) == null) {
                    throw new NonexistentEntityException("The sucursal with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sucursal sucursal;
            try {
                sucursal = em.getReference(Sucursal.class, id);
                sucursal.getIdSucursal();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sucursal with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Compra> compraListOrphanCheck = sucursal.getCompraList();
            for (Compra compraListOrphanCheckCompra : compraListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sucursal (" + sucursal + ") cannot be destroyed since the Compra " + compraListOrphanCheckCompra + " in its compraList field has a non-nullable idSucursal field.");
            }
            List<Vendedor> vendedorListOrphanCheck = sucursal.getVendedorList();
            for (Vendedor vendedorListOrphanCheckVendedor : vendedorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sucursal (" + sucursal + ") cannot be destroyed since the Vendedor " + vendedorListOrphanCheckVendedor + " in its vendedorList field has a non-nullable idSucursal field.");
            }
            List<Oferta> ofertaListOrphanCheck = sucursal.getOfertaList();
            for (Oferta ofertaListOrphanCheckOferta : ofertaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sucursal (" + sucursal + ") cannot be destroyed since the Oferta " + ofertaListOrphanCheckOferta + " in its ofertaList field has a non-nullable idSucursal field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Municipios idMunicipio = sucursal.getIdMunicipio();
            if (idMunicipio != null) {
                idMunicipio.getSucursalList().remove(sucursal);
                idMunicipio = em.merge(idMunicipio);
            }
            em.remove(sucursal);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sucursal> findSucursalEntities() {
        return findSucursalEntities(true, -1, -1);
    }

    public List<Sucursal> findSucursalEntities(int maxResults, int firstResult) {
        return findSucursalEntities(false, maxResults, firstResult);
    }

    private List<Sucursal> findSucursalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sucursal.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sucursal findSucursal(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sucursal.class, id);
        } finally {
            em.close();
        }
    }

    public int getSucursalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sucursal> rt = cq.from(Sucursal.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
