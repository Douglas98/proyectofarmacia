/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import com.entidades.Cliente;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.entidades.Tipocliente;
import com.entidades.Usuario;
import com.entidades.Venta;
import java.util.ArrayList;
import java.util.List;
import com.entidades.Tarjeta;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author samue
 */
public class ClienteJpaController implements Serializable {

    public ClienteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public ClienteJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoFinalFarmaciaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cliente cliente) throws PreexistingEntityException, Exception {
        if (cliente.getVentaList() == null) {
            cliente.setVentaList(new ArrayList<Venta>());
        }
        if (cliente.getTarjetaList() == null) {
            cliente.setTarjetaList(new ArrayList<Tarjeta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tipocliente idTipoCliente = cliente.getIdTipoCliente();
            if (idTipoCliente != null) {
                idTipoCliente = em.getReference(idTipoCliente.getClass(), idTipoCliente.getIdTipoCliente());
                cliente.setIdTipoCliente(idTipoCliente);
            }
            Usuario idUsuario = cliente.getIdUsuario();
            if (idUsuario != null) {
                idUsuario = em.getReference(idUsuario.getClass(), idUsuario.getIdUsuario());
                cliente.setIdUsuario(idUsuario);
            }
            List<Venta> attachedVentaList = new ArrayList<Venta>();
            for (Venta ventaListVentaToAttach : cliente.getVentaList()) {
                ventaListVentaToAttach = em.getReference(ventaListVentaToAttach.getClass(), ventaListVentaToAttach.getIdVenta());
                attachedVentaList.add(ventaListVentaToAttach);
            }
            cliente.setVentaList(attachedVentaList);
            List<Tarjeta> attachedTarjetaList = new ArrayList<Tarjeta>();
            for (Tarjeta tarjetaListTarjetaToAttach : cliente.getTarjetaList()) {
                tarjetaListTarjetaToAttach = em.getReference(tarjetaListTarjetaToAttach.getClass(), tarjetaListTarjetaToAttach.getIdtarjeta());
                attachedTarjetaList.add(tarjetaListTarjetaToAttach);
            }
            cliente.setTarjetaList(attachedTarjetaList);
            em.persist(cliente);
            if (idTipoCliente != null) {
                idTipoCliente.getClienteList().add(cliente);
                idTipoCliente = em.merge(idTipoCliente);
            }
            if (idUsuario != null) {
                idUsuario.getClienteList().add(cliente);
                idUsuario = em.merge(idUsuario);
            }
            for (Venta ventaListVenta : cliente.getVentaList()) {
                Cliente oldIdClienteOfVentaListVenta = ventaListVenta.getIdCliente();
                ventaListVenta.setIdCliente(cliente);
                ventaListVenta = em.merge(ventaListVenta);
                if (oldIdClienteOfVentaListVenta != null) {
                    oldIdClienteOfVentaListVenta.getVentaList().remove(ventaListVenta);
                    oldIdClienteOfVentaListVenta = em.merge(oldIdClienteOfVentaListVenta);
                }
            }
            for (Tarjeta tarjetaListTarjeta : cliente.getTarjetaList()) {
                Cliente oldIdClienteOfTarjetaListTarjeta = tarjetaListTarjeta.getIdCliente();
                tarjetaListTarjeta.setIdCliente(cliente);
                tarjetaListTarjeta = em.merge(tarjetaListTarjeta);
                if (oldIdClienteOfTarjetaListTarjeta != null) {
                    oldIdClienteOfTarjetaListTarjeta.getTarjetaList().remove(tarjetaListTarjeta);
                    oldIdClienteOfTarjetaListTarjeta = em.merge(oldIdClienteOfTarjetaListTarjeta);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCliente(cliente.getIdCliente()) != null) {
                throw new PreexistingEntityException("Cliente " + cliente + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cliente cliente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente persistentCliente = em.find(Cliente.class, cliente.getIdCliente());
            Tipocliente idTipoClienteOld = persistentCliente.getIdTipoCliente();
            Tipocliente idTipoClienteNew = cliente.getIdTipoCliente();
            Usuario idUsuarioOld = persistentCliente.getIdUsuario();
            Usuario idUsuarioNew = cliente.getIdUsuario();
            List<Venta> ventaListOld = persistentCliente.getVentaList();
            List<Venta> ventaListNew = cliente.getVentaList();
            List<Tarjeta> tarjetaListOld = persistentCliente.getTarjetaList();
            List<Tarjeta> tarjetaListNew = cliente.getTarjetaList();
            List<String> illegalOrphanMessages = null;
            for (Venta ventaListOldVenta : ventaListOld) {
                if (!ventaListNew.contains(ventaListOldVenta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Venta " + ventaListOldVenta + " since its idCliente field is not nullable.");
                }
            }
            for (Tarjeta tarjetaListOldTarjeta : tarjetaListOld) {
                if (!tarjetaListNew.contains(tarjetaListOldTarjeta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Tarjeta " + tarjetaListOldTarjeta + " since its idCliente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idTipoClienteNew != null) {
                idTipoClienteNew = em.getReference(idTipoClienteNew.getClass(), idTipoClienteNew.getIdTipoCliente());
                cliente.setIdTipoCliente(idTipoClienteNew);
            }
            if (idUsuarioNew != null) {
                idUsuarioNew = em.getReference(idUsuarioNew.getClass(), idUsuarioNew.getIdUsuario());
                cliente.setIdUsuario(idUsuarioNew);
            }
            List<Venta> attachedVentaListNew = new ArrayList<Venta>();
            for (Venta ventaListNewVentaToAttach : ventaListNew) {
                ventaListNewVentaToAttach = em.getReference(ventaListNewVentaToAttach.getClass(), ventaListNewVentaToAttach.getIdVenta());
                attachedVentaListNew.add(ventaListNewVentaToAttach);
            }
            ventaListNew = attachedVentaListNew;
            cliente.setVentaList(ventaListNew);
            List<Tarjeta> attachedTarjetaListNew = new ArrayList<Tarjeta>();
            for (Tarjeta tarjetaListNewTarjetaToAttach : tarjetaListNew) {
                tarjetaListNewTarjetaToAttach = em.getReference(tarjetaListNewTarjetaToAttach.getClass(), tarjetaListNewTarjetaToAttach.getIdtarjeta());
                attachedTarjetaListNew.add(tarjetaListNewTarjetaToAttach);
            }
            tarjetaListNew = attachedTarjetaListNew;
            cliente.setTarjetaList(tarjetaListNew);
            cliente = em.merge(cliente);
            if (idTipoClienteOld != null && !idTipoClienteOld.equals(idTipoClienteNew)) {
                idTipoClienteOld.getClienteList().remove(cliente);
                idTipoClienteOld = em.merge(idTipoClienteOld);
            }
            if (idTipoClienteNew != null && !idTipoClienteNew.equals(idTipoClienteOld)) {
                idTipoClienteNew.getClienteList().add(cliente);
                idTipoClienteNew = em.merge(idTipoClienteNew);
            }
            if (idUsuarioOld != null && !idUsuarioOld.equals(idUsuarioNew)) {
                idUsuarioOld.getClienteList().remove(cliente);
                idUsuarioOld = em.merge(idUsuarioOld);
            }
            if (idUsuarioNew != null && !idUsuarioNew.equals(idUsuarioOld)) {
                idUsuarioNew.getClienteList().add(cliente);
                idUsuarioNew = em.merge(idUsuarioNew);
            }
            for (Venta ventaListNewVenta : ventaListNew) {
                if (!ventaListOld.contains(ventaListNewVenta)) {
                    Cliente oldIdClienteOfVentaListNewVenta = ventaListNewVenta.getIdCliente();
                    ventaListNewVenta.setIdCliente(cliente);
                    ventaListNewVenta = em.merge(ventaListNewVenta);
                    if (oldIdClienteOfVentaListNewVenta != null && !oldIdClienteOfVentaListNewVenta.equals(cliente)) {
                        oldIdClienteOfVentaListNewVenta.getVentaList().remove(ventaListNewVenta);
                        oldIdClienteOfVentaListNewVenta = em.merge(oldIdClienteOfVentaListNewVenta);
                    }
                }
            }
            for (Tarjeta tarjetaListNewTarjeta : tarjetaListNew) {
                if (!tarjetaListOld.contains(tarjetaListNewTarjeta)) {
                    Cliente oldIdClienteOfTarjetaListNewTarjeta = tarjetaListNewTarjeta.getIdCliente();
                    tarjetaListNewTarjeta.setIdCliente(cliente);
                    tarjetaListNewTarjeta = em.merge(tarjetaListNewTarjeta);
                    if (oldIdClienteOfTarjetaListNewTarjeta != null && !oldIdClienteOfTarjetaListNewTarjeta.equals(cliente)) {
                        oldIdClienteOfTarjetaListNewTarjeta.getTarjetaList().remove(tarjetaListNewTarjeta);
                        oldIdClienteOfTarjetaListNewTarjeta = em.merge(oldIdClienteOfTarjetaListNewTarjeta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cliente.getIdCliente();
                if (findCliente(id) == null) {
                    throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente cliente;
            try {
                cliente = em.getReference(Cliente.class, id);
                cliente.getIdCliente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Venta> ventaListOrphanCheck = cliente.getVentaList();
            for (Venta ventaListOrphanCheckVenta : ventaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cliente (" + cliente + ") cannot be destroyed since the Venta " + ventaListOrphanCheckVenta + " in its ventaList field has a non-nullable idCliente field.");
            }
            List<Tarjeta> tarjetaListOrphanCheck = cliente.getTarjetaList();
            for (Tarjeta tarjetaListOrphanCheckTarjeta : tarjetaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cliente (" + cliente + ") cannot be destroyed since the Tarjeta " + tarjetaListOrphanCheckTarjeta + " in its tarjetaList field has a non-nullable idCliente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Tipocliente idTipoCliente = cliente.getIdTipoCliente();
            if (idTipoCliente != null) {
                idTipoCliente.getClienteList().remove(cliente);
                idTipoCliente = em.merge(idTipoCliente);
            }
            Usuario idUsuario = cliente.getIdUsuario();
            if (idUsuario != null) {
                idUsuario.getClienteList().remove(cliente);
                idUsuario = em.merge(idUsuario);
            }
            em.remove(cliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cliente> findClienteEntities() {
        return findClienteEntities(true, -1, -1);
    }

    public List<Cliente> findClienteEntities(int maxResults, int firstResult) {
        return findClienteEntities(false, maxResults, firstResult);
    }

    private List<Cliente> findClienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cliente findCliente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getClienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cliente> rt = cq.from(Cliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
